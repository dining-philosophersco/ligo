install ligo packages declared in package.json

ligo install \[PACKAGE_NAME\]

This command invokes the package manager to install the external
packages declared in package.json

=== flags ===

\[\--cache-path PATH\] The path where dependencies are installed.
\[-help\] print this help text and exit (alias: -?)
